import os
import pickle
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sn
import pandas as pd
from tqdm import tqdm
from keras.models import load_model
from sklearn.metrics import accuracy_score, confusion_matrix


os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'

def check_data():
    if os.path.isfile(config.p_path):
        print('Loading existing data for model')
        with open(config.p_path, 'rb') as handle:
            tmp = pickle.load(handle)
            return tmp
    else:
        return None


def check_pred_data():
    if os.path.isfile('predictions.csv'):
        print('Loading existinig prediction data')
        pred_df = pd.read_csv('predictions.csv')
        return pred_df
    else:
        return None


def build_predictions():
    tmp = check_data()
    if tmp:
        X_test = tmp.data[2]
        y_test = tmp.data[3]
    else:
        raise Exception('No test available')
        
    pred_data = check_pred_data()
    if not pred_data.empty:
        return X_test, pred_data.y_true, pred_data.y_pred
    
    y_pred = []
    for x in tqdm(X_test):
        x = x.reshape(1, x.shape[0], x.shape[1], 1)
        y_hat = model.predict(x)
        y_pred.append(np.argmax(y_hat))

    y_true = np.argmax(y_test, axis=1)
    y_true = y_true.tolist()
    y_pred = y_pred.tolist()
    
    pred_df = pd.DataFrame()
    pred_df['y_true'] = y_true
    pred_df['y_pred'] = y_pred
    pred_df.to_csv('predictions.csv', index=False)
    
    return X_test, y_true, y_pred


df = pd.read_csv('instruments.csv')
classes = list(np.unique(df.label))

p_path = os.path.join('pickles', 'conv.p')
with open(p_path, 'rb') as handle:
    config = pickle.load(handle)

model = load_model(config.model_path)

X_test, y_true, y_pred = build_predictions()


acc_score = accuracy_score(y_true=y_true, y_pred=y_pred)

# Plot confusion matrix
cfm = confusion_matrix(y_true, y_pred, normalize='pred')
df_cfm = pd.DataFrame(cfm, index = classes, columns = classes)
plt.figure(figsize = (10,7))
ax = sn.heatmap(df_cfm, annot=True, fmt='.3f')
ax.set(xlabel='Prediction data', ylabel='True data')

    











